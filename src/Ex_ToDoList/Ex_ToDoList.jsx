import React, { Component } from "react";
import { ThemeProvider } from "styled-components";
import { Container } from "../components/Container";
import { ToDoListDarkTheme } from "../Theme/ToDoListDarkTheme";
import { Dropdown } from "../components/Dropdown";
import {
  Heading1,
  Heading2,
  Heading3,
  Heading4,
  Heading5,
} from "../components/Heading";
import { TextField, lable, input } from "../components/TextField";
import { Button } from "../components/Button";
import { Table, Tr, Td, Th, Thead, Tbody } from "../components/Table";
import { connect } from "react-redux";
import {
  addTask_action,
  deleteTask_action,
  doneTask_action,
} from "../redux/action/ToDoListAction";
import { arrTheme } from "../Theme/ThemeManager";
import { change_theme } from "../redux/types/ToDoListType";
import { changeTheme_action } from "../redux/action/ToDoListAction";

class Ex_ToDoList extends Component {
  state = {
    taskName: "",
  };

  renderTaskToDo = () => {
    return this.props.taskList
      .filter((task) => !task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }} className="text-left">
              {task.taskName}
            </Th>
            <Th className="text-right">
              <Button>
                <i className="fa fa-edit"></i>
              </Button>

              <Button
                onClick={() => {
                  this.props.dispatch(doneTask_action(task.id));
                }}
              >
                <i className="fa fa-check"></i>
              </Button>

              <Button
                onClick={() => {
                  this.props.dispatch(deleteTask_action(task.id));
                }}
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderTaskComplete = () => {
    return this.props.taskList
      .filter((task) => task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }} className="text-left">
              {task.taskName}
            </Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.props.dispatch(deleteTask_action(task.id));
                }}
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderTheme = () => {
    return arrTheme.map((theme, index) => {
      return <option value={theme.id}>{theme.name}</option>;
    });
  };

  render() {
    return (
      <div>
        <ThemeProvider theme={this.props.themeToDoList}>
          <Container className="w-50">
            <Dropdown
              onChange={(e) => {
                let { value } = e.target;
                this.props.dispatch(changeTheme_action(value));
              }}
            >
              {this.renderTheme()}
            </Dropdown>

            <Heading3 className="mb-3 mt-3">To do list</Heading3>

            <TextField
              onChange={(e) => {
                this.setState({ taskName: e.target.value });
              }}
              label="TASK NAME"
            />

            <Button
              onClick={() => {
                let { taskName } = this.state;

                let newTask = {
                  id: Date.now(),
                  taskName: taskName,
                  done: false,
                };

                this.props.dispatch(addTask_action(newTask));
              }}
              className="ml-2"
            >
              <i className="fa fa-plus mr-1"></i>Add task
            </Button>

            <Button className="ml-2">
              <i className="fa fa-upload mr-1"></i>Update task
            </Button>

            <hr style={{ border: "1px solid white" }} className="mt-4" />
            <Heading3>Task to do</Heading3>

            <Table>
              <Thead>{this.renderTaskToDo()}</Thead>
            </Table>

            <Heading3>Task complete</Heading3>

            <Table>
              <Thead>{this.renderTaskComplete()}</Thead>
            </Table>
          </Container>
        </ThemeProvider>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    themeToDoList: state.ToDoListReducer.themeToDoList,
    taskList: state.ToDoListReducer.taskList,
  };
};

export default connect(mapStateToProps)(Ex_ToDoList);
