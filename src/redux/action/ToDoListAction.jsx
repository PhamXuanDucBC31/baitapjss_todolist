import {
  add_task,
  change_theme,
  delete_task,
  done_task,
} from "../types/ToDoListType";

export const addTask_action = (newTask) => ({
  type: add_task,
  newTask,
});
export const changeTheme_action = (themeId) => ({
  type: change_theme,
  themeId,
});
export const doneTask_action = (taskId) => ({
  type: done_task,
  taskId,
});
export const deleteTask_action = (taskId) => ({
  type: delete_task,
  taskId,
});
