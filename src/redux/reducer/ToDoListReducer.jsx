import { ToDoListPrimaryTheme } from "../../Theme/ToDoListPrimaryTheme";
import {
  add_task,
  change_theme,
  delete_task,
  done_task,
} from "../types/ToDoListType";
import { arrTheme } from "../../Theme/ThemeManager";

const initialState = {
  themeToDoList: ToDoListPrimaryTheme,
  taskList: [
    { id: 1, taskName: "task1", done: true },
    { id: 2, taskName: "task2", done: true },
    { id: 3, taskName: "task3", done: false },
    { id: 4, taskName: "task4", done: true },
    { id: 5, taskName: "task5", done: false },
    { id: 6, taskName: "task6", done: true },
  ],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case add_task: {
      let taskListUpdate = [...state.taskList];

      let index = taskListUpdate.findIndex(
        (task) => task.taskName === action.newTask.taskName
      );

      if (index !== -1) {
        alert("taskName already exists");
        return { ...state };
      }

      taskListUpdate.push(action.newTask);
      state.taskList = taskListUpdate;
      return { ...state };
    }

    case change_theme: {
      let theme = arrTheme.find((theme) => theme.id == action.themeId);

      if (theme) {
        state.themeToDoList = { ...theme.theme };
      }

      return { ...state };
    }

    case done_task: {
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex((task) => task.id === action.taskId);
      if (index !== -1) {
        taskListUpdate[index].done = true;
      }

      return { ...state, taskList: taskListUpdate };
    }

    case delete_task: {
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex((task) => task.id === action.taskId);
      if (index !== -1) {
        taskListUpdate.splice(index, 1);
      }
      return { ...state, taskList: taskListUpdate };
    }

    default:
      return state;
  }
};
