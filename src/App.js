import logo from "./logo.svg";
import "./App.css";
import Ex_ToDoList from "./Ex_ToDoList/Ex_ToDoList";

function App() {
  return (
    <div className="App">
      <Ex_ToDoList />
    </div>
  );
}

export default App;
