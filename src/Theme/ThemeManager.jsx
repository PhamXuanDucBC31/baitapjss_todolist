import { ToDoListDarkTheme } from "./ToDoListDarkTheme";
import { ToDoListLightTheme } from "./ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "./ToDoListPrimaryTheme";

export const arrTheme = [
  {
    id: 1,
    name: "Primary theme",
    theme: ToDoListPrimaryTheme,
  },
  {
    id: 2,
    name: "Light theme",
    theme: ToDoListLightTheme,
  },
  {
    id: 3,
    name: "Dark theme",
    theme: ToDoListDarkTheme,
  },
];
